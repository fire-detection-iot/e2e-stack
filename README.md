# End to End stack on React IoT application

The stack consists of 3 parts: broker, backend and frontend. To start the entire stack locally, follow the below steps:

1. Navigate to `broker` folder and run `docker-compose up -d`. This starts up the MQTT server irrespective of the OS. Alternatively, running `runMqttBrokerAdvanced.bat` on Windows executable also achieves the same. Running the `runMqttSpy.bat` executable stands up a mqtt spy application on Windows.
2. Navigate to `frontend` folder, install the dependencies using `npm install` and run `npm start`. React frontend gets connected to the MQTT broker setup in the previous step.
3. Run the script `script_local.py`, after setting up the virtual env and installing the requirements (refer the README.md of `backend` folder for setting up the backend project) to publish dummy values onto the MQTT broker. Successful connection can be verified by the alternating texts `Happy Home!!` and `Fire in the house!!` on the React application.

_Opening the project in gitpod, sets up the whole infrastructure and stands up the stack. Wait for a couple of minutes after the react application startup, as the react application started in gitpod takes a minute or two for it to get connected to the MQTT broker in gitpod docker._

## Broker (for local setup)

With the help of docker compose, an MQTT docker container gets created locally with MQTT port at 1884 and websockets port at 9001.

## Backend

The backend folder houses 2 scripts: `script_local.py` and `script_aws.py` for local and cloud connections respectively. Running `script_local.py` publishes payload with true and false values alternatively. This script is used for testing local setup even without the IoT hardware. The `script_aws.py` has bearings for connecting to IoT hardware and AWS cloud. Additionally, the scripts `script_aws_sdk_v1.py` and `script_aws_sdk_v2.py` are used for testing connection with AWS using v1 and v2 versions of the python SDK.

## Frontend

For frontend to work with the latest updates in webpack v5, some changes have to be made `config/webpack.config.js`. _These changes are already incorporated. No further actions required from the developer._
1. Install `node-polyfill-webpack-plugin`.
2. Add the import `const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")`.
3. Add the below plugin in `plugins` key,
    ```
    new NodePolyfillPlugin({
        excludeAliases: ['console'],
    }),
    ```
    The `excludeAliases` key is added to solve the import error for `console-broswerify`. (Refer https://stackoverflow.com/questions/73042033/you-attempted-to-import-node-modules-console-browserify-index-js-which-falls)
4. Add the below fallback key in resolve key,
    ```
    fallback: {
        "buffer": require.resolve("buffer/"),
        "stream": require.resolve("stream-browserify"),
        "process": require.resolve("process/browser"),
        "path": require.resolve("path-browserify"),
        "fs": false,
        "tls": false,
        "console": require.resolve("console-browserify"),
    },
    ```

To identify the correct locations, cross refernce these keys in the `config/webpack.config.js` file.

With these changes, the React project is compatible with the MQTT library. 

The project is designed to run in local mode (desktop and gitpod) as well as cloud mode with AWS. The desktop local mode can be started with `npm start` or `npm run start:local` and the local gitpod mode started using `npm run start:local-gitpod`. The AWS MQTT mode can be started by running `npm run start:dev1`.

Refer the package.json for the different startup and deployment modes.

The frontend library for AWS MQTT is also at v2 at the point of documenting this, however, the code is still using v1. v2 version is yet to be explored and demonstrated.

