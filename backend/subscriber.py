
import paho.mqtt.client as mqtt 
from time import sleep
import time
import json


# Custom MQTT message callback
def on_message(client, userdata, message):
    print("Message received: "  , json.loads(message.payload.decode('UTF-8'))["status"])

def on_connect(client, userdata, flags, rc):  
    if rc == 0:  
        print("Connected to broker")  
        global Connected                #Use global variable
        Connected = True                #Signal connection   
    else:  
        print("Connection failed")

Connected = False
host = "127.0.0.1"
port = 1884
clientId = "LocalPythonSubscriber"
topic = "fire-event-topic"

# Init MQTTClient
myLocalMQTTClient = None
myLocalMQTTClient = mqtt.Client(clientId)
myLocalMQTTClient.on_connect= on_connect                      #attach function to callback
myLocalMQTTClient.on_message= on_message
# Connect and subscribe to MQTT Broker
myLocalMQTTClient.connect(host,port)

myLocalMQTTClient.loop_start()        #start the loop
  
while Connected != True:    #Wait for connection
    time.sleep(0.1)
  
myLocalMQTTClient.subscribe(topic)
  
try:
    while True:
        time.sleep(1)
  
except KeyboardInterrupt:
    print("exiting")
    client.disconnect()
    client.loop_stop()
