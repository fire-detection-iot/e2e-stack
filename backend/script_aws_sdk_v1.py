
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from pyfirmata import Arduino, util
from time import sleep
import logging
import time
import json

# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")



host = "a1mo15cnv3na9p-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certificates/LAFVIN/AmazonRootCA1.pem"
certificatePath = "certificates/LAFVIN/LAFVIN-certificate.pem.crt"
privateKeyPath = "certificates/LAFVIN/LAFVIN-private.pem.key"
port = 8883
clientId = "LAFVIN"
topic = "fire-event-topic"
opMode = "publish"


# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(host, port)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
print("Connected to Arduino")

if opMode == 'both' or opMode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
time.sleep(2)

#fire_sensor_pin.enable_reporting()

message = {}
message['status'] = False  
while True:         
    message['status'] = not message['status']
    messageJson = json.dumps(message)
    messageStr = str(messageJson)
    myAWSIoTMQTTClient.publish(topic, messageStr, 1)                  
    print('Publishing to the topic %s: %s\n' % (topic, messageStr))
    time.sleep(3)