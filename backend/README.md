# Backend

## Setting up in different OSs

### Windows

1. `python -m venv <path to env>` -> `python -m venv virtual_env`
2. `<path to env>\Scripts\activate.bat` -> `virtual_env\Scripts\activate.bat`
3. `python -m pip install -r requirements.txt`
4. `<path to env>\Scripts\deactivate.bat` -> `virtual_env\Scripts\deactivate.bat`

### Unix

1. `python -m venv <path to env>` -> `python -m venv virtual_env`
2. `. <path to env>/bin/activate` -> `. virtual_env/bin/activate`
3. `python -m pip install -r requirements.txt`
4. `deactivate`

## Execution

1. Local mode : `python script_local.py` 
2. AWS mode :  `python script_aws.py` 

## AWS Thing and script setup

1. Register the thing in AWS IoT core. While registering, for simplicity, provide the option to create certificate by AWS itself.
2. Download the certificate and key files generated as part of the above step. These steps take care of the authentication aspect for any hardware thing. _The existing pem and crt files inside the `certificates` folder are placeholder files with empty contents._
3. Create AWS policy (authorization) for the certificate created by following and tweaking the `certPolicyTemplate.json` present in `policies/LAFVIN` folder.
4. The downloaded RootCA1 file, certificate file and private file need to be placed in certificates folder and the correct location need to be provided in the aws related script files (`script_aws.py`, `script_aws_sdk_v1.py`, `script_aws_sdk_v2.py`).
5. The `ENDPOINT` variable in the script files should be changed to the value obtained from the `Endpoint` value in `Settings` section in `AWS IoT Core`.

## References

1. https://docs.aws.amazon.com/iot/latest/developerguide/example-iot-policies.html
2. https://repost.aws/knowledge-center/iot-core-publish-mqtt-messages-python
3. https://theskenengineering.com/building-a-react-js-app-with-aws-iot/
