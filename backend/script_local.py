
import paho.mqtt.client as mqtt 
from time import sleep
import time
import json


# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")



host = "127.0.0.1"
port = 1884
clientId = "LocalPython"
topic = "fire-event-topic"

# Init MQTTClient
myLocalMQTTClient = None
myLocalMQTTClient = mqtt.Client(clientId)

# Connect and subscribe to MQTT Broker
myLocalMQTTClient.connect(host,port)

myLocalMQTTClient.subscribe(topic, 1, customCallback)

message = {}
message['status'] = False  
while True:         
    message['status'] = not message['status']
    messageJson = json.dumps(message)
    messageStr = str(messageJson)
    myLocalMQTTClient.publish(topic, messageStr, 1)                  
    print('Publishing to the topic %s: %s\n' % (topic, messageStr))
    time.sleep(3)




