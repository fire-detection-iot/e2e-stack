
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from pyfirmata import Arduino, util
from time import sleep
import logging
import time
import json

AllowedActions = ['both', 'publish', 'subscribe']

board = Arduino('COM3')
it = util.Iterator(board)
it.start()
fire_sensor_pin = board.get_pin('a:5:i')


# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")



ENDPOINT = "<ENDPOINT_value_from_Settings_IoTCore>"
rootCAPath = "certificates/LAFVIN/AmazonRootCA1.pem"
certificatePath = "certificates/LAFVIN/LAFVIN-certificate.pem.crt"
privateKeyPath = "certificates/LAFVIN/LAFVIN-private.pem.key"
port = 8883
clientId = "LAFVIN"
topic = "fire-event-topic"
opMode = "publish"


# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, port)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
print("Connected to Arduino")

if opMode == 'both' or opMode == 'subscribe':
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
time.sleep(2)

#fire_sensor_pin.enable_reporting()

message = {}
message['status'] = False  
while True:
    if opMode == 'both' or opMode == 'publish':  
        
        # if fire_sensor_pin.read() == 1:
        #     message['status'] = True
        #     board.digital[12].write(1)
        #     board.digital[10].write(1)
        # else:
        print(fire_sensor_pin.read())
        if fire_sensor_pin.read() >= 0.1:            
            message['status'] = True
            board.digital[13].write(1)
            board.digital[12].write(1)
            board.digital[11].write(0)
        else:
            message['status'] = False
            board.digital[13].write(0)
            board.digital[12].write(0)
            board.digital[11].write(1)
               
        messageJson = json.dumps(message)
        myAWSIoTMQTTClient.publish(topic, messageJson, 1)                  
        print('Publishing to the topic %s: %s\n' % (topic, messageJson))
    time.sleep(1)




