@echo off
set brokerName=local-mqtt-broker
echo "Welcome to the MQTT broker creator facility.."
for /f %%i in ('docker ps -qf "name=^%brokerName%"') do set containerId=%%i
if "%containerId%" == "" (
	setlocal enabledelayedexpansion
	for /f %%i in ('docker ps -aq -f "status=exited" -f "name=^%brokerName%"') do (		
		set exitedContainerId=%%i			
	)   
	if NOT "!exitedContainerId!" =="" ( 
		echo "Starting the exited container..."		
		docker start !exitedContainerId!			
	) else (
		set configmount=!cd!\mosquitto\config
		set datamount=!cd!\mosquitto\data
		set logmount=!cd!\mosquitto\log
		set credentialsmount=!cd!\mosquitto\credentials
		set imageName=eclipse-mosquitto:2.0.14
		for /f %%i in ('docker image inspect "!imageName!"') do set imageId=%%i
		IF !imageId! == []  (
			echo "No image present. Pulling !imageName! now.."
			docker pull !imageName!
		) else (
			echo "Image already existing. Proceeding to running a fresh container.."
		)
		docker run -it -d --name local-mqtt-broker -p 1884:1883 -p 9001:9001 -v "!configmount!":/mosquitto/config/ -v "!datamount!":/mosquitto/data/ -v "!logmount!":/mosquitto/log/ -v "!credentialsmount!":/etc/mosquitto/ !imageName!
	)	
    echo "MQTT Broker has started"
	endlocal
) else (
	echo "MQTT Broker already running"
)
