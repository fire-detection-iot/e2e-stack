@echo off
set configmount=%cd%\mosquitto\config
set datamount=%cd%\mosquitto\data
set logmount=%cd%\mosquitto\log
set credentialsmount=%cd%\mosquitto\credentials
docker run -it -d --name local-mqtt-broker -p 1883:1883 -p 9001:9001 -v "%configmount%":/mosquitto/config/ -v "%datamount%":/mosquitto/data/ -v "%logmount%":/mosquitto/log/ -v "%credentialsmount%":/etc/mosquitto/ eclipse-mosquitto

