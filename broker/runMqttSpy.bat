@echo off
IF EXIST mqtt-spy/mqtt-spy.jar (
    echo MQTT Spy jar exists.
) ELSE (
	echo First time run! Downoading the MQTT Spy jar.
	mkdir mqtt-spy
	curl -o mqtt-spy/mqtt-spy.jar -LJO --ssl-no-revoke https://github.com/eclipse/paho.mqtt-spy/releases/download/1.0.1-beta18/mqtt-spy-1.0.1-beta-b18-jar-with-dependencies.jar    
)
echo Starting MQTT Spy.
cd mqtt-spy
java -jar mqtt-spy.jar
cd ..