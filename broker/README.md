## Setup

### Setting up in different OSs

In Windows and Unix, Running the docker-compose file using the command `docker-compose up -d` can setup the MQTT broker. Alternatively, in Windows, running `runMqttBrokerAdvanced.bat` can also achieve the same. On Windows, running `runMqttSpy.bat` can bring up a GUI tool for monitoring and interacting with MQTT topics.

No matter how the broker is started, the final broker is a docker container. The container depends on the volume named `mosquitto` which house the configurations for the broker.

## Notes

MQTT port is exposed at 1884 and Websockets port at 9001.