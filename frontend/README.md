## Setup

Run, `npm install` in the frontend folder to install the dependencies. After the installation, the application can be started in multiple modes.

1. Local mode: `npm start` or `npm run start:local`
2. Gitpod local mode: `start:local-gitpod`
3. AWS MQTT mode: `start:dev1`

## Notes

Currently, the frontend is designed such that it can work from anywhere and can communicate with local MQTT as well as AWS MQTT. _The amplify part of deployment is not yet fully developed and tested. It is just there as a placeholder for future exploration._

In the `.env.dev*` files, 
1. `REACT_APP_MQTT_ID` is the first part of the Endpoint value from Settings section of AWS IoT Core. 
2. `REACT_APP_CLIENT_ID` is just a name of the connecting frontent client.

## References

https://github.com/mqttjs/MQTT.js

https://stackoverflow.com/questions/73042033/you-attempted-to-import-node-modules-console-browserify-index-js-which-falls