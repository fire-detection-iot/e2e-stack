import './App.css';
// import AppAWSwithSDK from './AppAWSwithSDK';
// import AppAWSwithAmplify from './AppAWSwithAmplify';
import AppLocal from './AppLocal';

function App() {
  return (  
    <>
      {process.env.REACT_APP_ENV.toLowerCase() =="local" && 
        <AppLocal  />
      }
      {/* {process.env.REACT_APP_ENV.toLowerCase() =="dev1" && 
        <AppAWSwithSDK  />
      } */}
      {/* {process.env.REACT_APP_ENV.toLowerCase() =="dev2" && 
        <AppAWSwithAmplify  />
      } */}
    </>
  );
}

export default App;
