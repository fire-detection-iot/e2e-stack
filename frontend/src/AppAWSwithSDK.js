import React, {useState} from "react";

import * as AWS from 'aws-sdk';
import * as AWSIoTData from 'aws-iot-device-sdk';

import AppPanel from './AppPanel';


let awsConfig = {
  identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID,
  mqttEndpoint: `${process.env.REACT_APP_MQTT_ID}.iot.${process.env.REACT_APP_REGION}.amazonaws.com`,
  region: process.env.REACT_APP_REGION,
  clientId: process.env.REACT_APP_CLIENT_ID,
  userPoolId: process.env.REACT_APP_USER_POOL_ID
};

const client = AWSIoTData.device({
  region: awsConfig.region,
  host: awsConfig.mqttEndpoint,
  clientId: awsConfig.clientId,
  protocol: 'wss',
  maximumReconnectTimeMs: 8000,
  debug: false,
  accessKeyId: '',
  secretKey: '',
  sessionToken: ''
});

AWS.config.region = awsConfig.region;

AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: awsConfig.identityPoolId
});

AWS.config.credentials.get((err) => {
    if (err) {
        // console.log(AWS.config.credentials);
        throw err;
    } else {
        client.updateWebSocketCredentials(
            AWS.config.credentials.accessKeyId,
            AWS.config.credentials.secretAccessKey,
            AWS.config.credentials.sessionToken
        );
    }
});

client.on('connect', () => {
  console.log('Connected')
  client.subscribe(process.env.REACT_APP_TOPIC_NAME,(err, granted)=>{
    // client.publish(process.env.REACT_APP_TOPIC_NAME,'{"status": true}',(err,)=>{
    //   if(!err){
    //     console.log('published')
    //   }
    // })
    console.log('Subscribed')
  })
  
});


function AppAWSwithSDK() {

  const [sensorStatus, setSensorStatus] = useState(false);

  client.on('message', (topic, message ) => {
    const remoteSensorStatus = JSON.parse(message.toString()).status;
    if(remoteSensorStatus!=sensorStatus){
      setSensorStatus(remoteSensorStatus);
    }  
  });

  return (  
    <AppPanel sensorStatus={sensorStatus} />
  );
}

export default AppAWSwithSDK;
