import React, {useState} from "react";
import {Amplify} from 'aws-amplify';
import { AWSIoTProvider } from '@aws-amplify/pubsub/lib/Providers';
import AppPanel from './AppPanel';

Amplify.configure({
  Auth: {
    identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID,
    region: process.env.REACT_APP_REGION,
    userPoolId: process.env.REACT_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_WEB_CLIENT_ID
  }
});

Amplify.addPluggable(new AWSIoTProvider({
  aws_pubsub_region: process.env.REACT_APP_REGION,
  aws_pubsub_endpoint: `wss://${process.env.REACT_APP_MQTT_ID}.iot.${process.env.REACT_APP_REGION}.amazonaws.com/mqtt`,
}));



function AppAWSwithAmplify() {

  const [sensorStatus, setSensorStatus] = useState(false);

  Amplify.PubSub.subscribe(process.env.REACT_APP_TOPIC_NAME).subscribe({
    next: data => {
      const remoteSensorStatus = JSON.parse(data.toString()).status;
      if(remoteSensorStatus!=sensorStatus){
        setSensorStatus(remoteSensorStatus);
      }      
    },
    error: error => console.error(error),
    close: () => console.log('Done'),
  });

  return (  
    <AppPanel sensorStatus={sensorStatus} />
  );
}

export default AppAWSwithAmplify;
