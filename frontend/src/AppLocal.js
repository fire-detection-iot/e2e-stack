import React,  {useState, useEffect} from 'react';
import AppPanel from './AppPanel';
import * as mqtt from "mqtt";

const host=process.env.REACT_APP_LOCAL_MQTT_IP;
const port=process.env.REACT_APP_LOCAL_MQTT_PORT;
const protocol=process.env.REACT_APP_LOCAL_MQTT_PROTOCOL;
// const connectUrl = `wss://${host}:${port}`
// In case of urls with no port, the connect url will look like below
// const connectUrl = "wss://9001-firedetectioni-e2estack-aaaaaaaaaaa.ws-us111.gitpod.io"
const connectUrl = `${protocol}://${host}${port=="" ? "":":" + port}`
let localConfig = {
  clientId: process.env.REACT_APP_LOCAL_CLIENT_ID,
  clean: true,
  connectTimeout: 4000,
  protocol: `${protocol}`,
  protocolId: 'MQIsdp', protocolVersion: 3,
  // username: 'emqx',
  // password: 'public',
  reconnectPeriod: 1000,
};
const client = mqtt.connect(connectUrl,localConfig);
client.on('connect', () => {
  console.log('Connected')
  client.subscribe(process.env.REACT_APP_TOPIC_NAME,(err, granted)=>{
    // client.publish(process.env.REACT_APP_TOPIC_NAME,'{"status": true}',(err,)=>{
    //   if(!err){
    //     console.log('published')
    //   }
    // })
    console.log('Subscribed')
  })
  
});
// client.on('message', (topic, message) => {
//   console.log('outside',JSON.parse(message.toString()).status);
// });

export default () => {
  const [sensorStatus, setSensorStatus] = useState(false);

  client.on('message', (topic, message ) => {
    const remoteSensorStatus = JSON.parse(message.toString()).status;
    if(remoteSensorStatus!=sensorStatus){
      setSensorStatus(remoteSensorStatus);
    }  
  });

  return (  
    <AppPanel sensorStatus={sensorStatus} />
  );
}