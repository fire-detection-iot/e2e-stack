

function AppPanel({sensorStatus}) {

    return (  
      <div className="custom-center">
        <h1>{sensorStatus?<span className="fire">Fire in the House!!</span>:<span className="safe">Happy Home!!</span>}</h1>
      </div>
    );
  }
  
  export default AppPanel;
  